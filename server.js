require('dotenv').config()
process.title = 'socket-io-server';

var express = require('express');
var app = express();

var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

var redis = require("redis");

server.listen(process.env.PORT, function () {
  console.log('Example app listening on port 3001!')
});

app.use(function(req, res, next){
  console.log('%s %s', req.method, req.url);
  next();
});

io.sockets.on('connection', function (socket) {
  var subscribe = redis.createClient(process.env.REDISTOGO_URL);
  subscribe.subscribe('message.create');

  subscribe.on("message", function(channel, message) {
    var messageJSON = JSON.parse(message)
    
    console.log("from rails to subscriber:", channel, message);
    
    console.log('*********************************')
    console.log('chat_message_group_' + messageJSON.message_group_id)
    console.log('*********************************')

    socket.emit('chat_message_group_' + messageJSON.message_group_id, message)
  });

  // unsubscribe from redis if session disconnects
  socket.on('disconnect', function () {
    console.log("user disconnected");

    subscribe.quit();
  });

});